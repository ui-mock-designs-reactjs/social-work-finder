import { TypedQueryDocumentSnapshot } from "typesafe-firestore";
import { firestore } from "./firebase";

export interface iBusiness {
  name: string;
  createdBy: string;
}

export interface iBusinessWithId {
  id: string;
  name: string;
}

export const collectIdsAndDocs = (
  docs: TypedQueryDocumentSnapshot<iBusiness>[]
): iBusinessWithId[] => {
  return docs.map((doc) => {
    return {
      id: doc.id,
      ...doc.data(),
    };
  });
};

export const getUserDocument = async (uid: any) => {
  if (!uid) return null;

  try {
    const userDocument = await firestore.collection("users").doc(uid).get();
    console.log("User Document: ", uid, userDocument.data());
    return {
      uid,
      ...userDocument.data(),
    };
  } catch (err) {
    console.error(err);
  }
};

export const createUserProfileDocuement = async (
  user: any,
  additionalData: any
) => {
  if (!user) return;

  const { displayName, email, photoURL } = user;

  const userRef = firestore.doc(`users/${user.uid}`);
  const snapShot = await userRef.get();

  if (snapShot.exists) {
    return {
      uid: user.uid,
      ...snapShot.data(),
    };
  }

  const createdAt = new Date();
  try {
    await userRef.set({
      displayName,
      email,
      photoURL,
      createdAt,
      ...additionalData,
    });
  } catch (err) {
    console.error(err);
  }

  return getUserDocument(user.uid);
};
