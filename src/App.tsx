import React, { useEffect, useState } from "react";
import "./App.css";
import { FullScreenLoader } from "./components/full-screen-loader/full-screen-loader";
import { Header } from "./components/header/header";
import firebase, {
  auth,
  firestore,
  signInWithGoogle,
  signOut,
} from "./firebase";
import { BusinessesScreen } from "./screens/businesses/businesses";
import { iUser } from "./types/iUser";
import { createUserProfileDocuement } from "./utilities";
// import { User } from "firebase";
(window as any).firebase = firebase;
(window as any).firestore = firestore;

function App() {
  const [user, setUser] = useState<iUser | null>();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    auth.onAuthStateChanged(async (authUser: firebase.UserInfo | null) => {
      if (authUser) {
        await createUserProfileDocuement(authUser, {});
        const { uid, displayName, email, photoURL } = authUser;
        setUser({
          uid,
          email: email || "",
          photoURL: photoURL || "",
          displayName: displayName || "",
        });
      } else {
        setUser(null);
      }
      setTimeout(() => setLoading(false), 2000);
    });
  }, []);

  const loginWithGoogle = () => {
    signInWithGoogle();
  };

  if (loading) {
    return <FullScreenLoader />;
  }

  return (
    <div className="App">
      {!user && (
        <div>
          <button onClick={loginWithGoogle}>Sign In with Google</button>
        </div>
      )}

      {user && <Header user={user} />}

      {user && <BusinessesScreen uid={user!.uid} />}
    </div>
  );
}

export default App;
