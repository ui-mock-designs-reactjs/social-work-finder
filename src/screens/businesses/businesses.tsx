import React, { useEffect, useState } from "react";
import { auth, firestore } from "../../firebase";
import {
  TypedCollectionReference as CollectionReference,
  TypedQueryDocumentSnapshot as QueryDocumentSnapshot,
  TypedQueryDocumentSnapshot,
} from "typesafe-firestore";
import "./businesses.scss";
import { iBusiness, iBusinessWithId } from "../../utilities";
import { SearchBar } from "../../components/search-bar/search-bar";
import { BusinessListItem } from "../../components/business-list-item/business-list-item";
import { FullScreenLoader } from "../../components/full-screen-loader/full-screen-loader";

// Collection of the Business entities
// This is just a reference to the collection
// and does not fetch the data until we call the
// .get() method on it which returns a QuerySnapshot Promise
const BusinessCollection = firestore.collection(
  "businesses"
) as CollectionReference<iBusiness>;

interface Props {
  uid: string;
}

export const BusinessesScreen = (props: Props) => {
  const [businesses, setBusinesses] =
    useState<QueryDocumentSnapshot<iBusiness>[]>();

  const [business, setBusiness] = useState<string>("");

  const deleteBusiness = (id: string) => {
    BusinessCollection.doc(id).delete();
  };

  useEffect(() => {
    const unsubscribe = BusinessCollection.onSnapshot((snapshot) => {
      setBusinesses(snapshot.docs as TypedQueryDocumentSnapshot<iBusiness>[]);
    });
    return unsubscribe;
  }, []);

  const createBusiness = () => {
    if (business.trim() != "") {
      BusinessCollection.add({
        name: business.trim(),
        createdBy: auth.currentUser?.uid || "",
      });
    }

    setBusiness("");
  };

  

  if (!businesses) {
    return <FullScreenLoader />;
  }

  return (
    
    <div className="businesses">
      <div className="businesses-sidebar">
        <SearchBar />
        {/* <div className="create-form">
          <input
            value={business}
            onKeyUp={(e) => e.key === "Enter" && createBusiness()}
            onChange={(e) => setBusiness(e.target.value)}
            type="text"
          />
          <button onClick={createBusiness}>Create</button>
        </div> */}
        <div className="business-list">
          {businesses.map((business) => {
            const data = business.data();
            return (
              <BusinessListItem business={business} />
            );
          })}
        </div>
      </div>
    </div>
  );
};
