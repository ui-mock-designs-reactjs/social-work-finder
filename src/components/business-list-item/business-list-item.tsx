import React from "react";
import { TypedQueryDocumentSnapshot } from "typesafe-firestore";
import { iBusiness } from "../../utilities";
import "./business-list-item.scss";

interface Props {
  business: TypedQueryDocumentSnapshot<iBusiness>;
}

const alphabet_colors = {
  a: "#f44336",
  b: "#f44336",
  c: "#f44336",
  d: "#f44336",
  e: "#f44336",
  f: "#9c27b0",
  g: "#9c27b0",
  h: "#9c27b0",
  i: "#9c27b0",
  j: "#9c27b0",
  k: "#3f51b5",
  l: "#3f51b5",
  m: "#3f51b5",
  n: "#3f51b5",
  o: "#3f51b5",
  p: "#2196f3",
  q: "#2196f3",
  r: "#2196f3",
  s: "#2196f3",
  t: "#2196f3",
  u: "#009688",
  v: "#009688",
  w: "#009688",
  x: "#009688",
  y: "#009688",
  z: "#009688",
};

export const BusinessListItem = (props: Props) => {
  const { business } = props;
  const data = business.data();
  const background = (alphabet_colors as any)[data.name[0].toLowerCase()] || "#ff9800";
  
  return (
    <div className="business-item" key={business.id}>
      <div className="left">
        <div style={{ background }} className="initial">
          {data.name[0]}
        </div>
      </div>
      <div className="right">
        <div className="business-name">{data.name}</div>
        <div className="business-tags">
          <div className="tag">education</div>
          <div className="tag">children</div>
          <div className="tag">environment</div>
        </div>
      </div>
    </div>
  );
};
