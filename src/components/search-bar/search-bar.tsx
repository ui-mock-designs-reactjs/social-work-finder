import React from 'react';
import { FilterOutlined, SearchOutlined } from '@ant-design/icons';
import './search-bar.scss';

export const SearchBar = () => {
    return (
        <div className="search-bar">
            <div className="search-box">
                <SearchOutlined />
                <input placeholder="Search" spellCheck="false" className="search-field" type="text" />
            </div>
            {/* <div className="filter-button">
                <FilterOutlined />
            </div> */}
        </div>
    )
}