import React from "react";
import { iUser } from "../../types/iUser";
import "./header.scss";
import logo from "../../images/aasha.png";
import { BellFilled, PoweroffOutlined, SettingFilled } from "@ant-design/icons";
import { signOut } from "../../firebase";

interface Props {
  user: iUser;
}

export const Header = (props: Props) => {
  return (
    <div className="header">
      <div className="left">
        <img className="logo" src={logo} />
        {/* <div className="searchbar">
          <input type="text" placeholder="Search" />
        </div> */}
      </div>

      <div className="right">
        <div className="header-icons">
          <div className="header-icon" onClick={() => signOut()}>
            <PoweroffOutlined />
          </div>

          <div className="header-icon">
            <BellFilled />
          </div>

          <div className="header-icon">
            <SettingFilled />
          </div>
        </div>
        {/* <div className="name">{props.user.displayName}</div> */}
        <div className="avatar">
          {/* <img src={props.user.photoURL} /> */}
          <div className="initial">{props.user.displayName[0]}</div>
        </div>
      </div>
    </div>
  );
};
