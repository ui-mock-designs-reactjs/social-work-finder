import React from "react";
import "./full-screen-loader.scss";
import loadingcat from "../../images/loadingcat.gif";

export const FullScreenLoader = () => {
  return (
    <div className="full-screen-loader">
      <img src={loadingcat} />
    </div>
  );
};
