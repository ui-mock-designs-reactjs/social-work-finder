// Structure of user returned
// from Google OAuth login
export interface iUser {
  uid: string;
  email: string;
  photoURL: string;
  displayName: string;
}
